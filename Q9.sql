SELECT
 t.category_name,
  SUM(t1.item_price)AS total_price 
FROM
  item_category t 
INNER JOIN
 item  t1 
ON 
  t.category_id = t1.category_id
GROUP BY
  t.category_id
ORDER BY
 total_price DESC;

